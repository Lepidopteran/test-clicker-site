const gulp = require('gulp');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const postcssPresetEnv = require('postcss-preset-env');
const browserSync = require('browser-sync').create();

gulp.task('css', function () {
  return gulp
    .src('src/styles/*.css')
    .pipe(postcss([autoprefixer(), postcssPresetEnv(), cssnano()]))
    .pipe(gulp.dest('res/styles/'))
    .pipe(browserSync.stream());
});

gulp.task('serve', function () {
  browserSync.init({
    server: '.',
  });

  gulp.watch('src/styles/*.css', gulp.parallel('css'));
  gulp.watch('*.html').on('change', browserSync.reload);
});

gulp.task('default', gulp.parallel('css', 'serve'));
